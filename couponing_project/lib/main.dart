import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:mspr/Code.dart';
import 'package:qr_flutter/qr_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  build(context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My Http App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State {
  List<Code> codes = List();
  String result = "";
  var isLoading = false;

  getCodes() async {
    setState(() {
      isLoading = true;
    });

    final response =
        await http.get("https://apiqrcode.azurewebsites.net/api/reduction/all");

    if (response.statusCode == 200) {
      codes = (json.decode(response.body) as List)
          .map((data) => new Code.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to fetch codes');
    }
  }

  initState() {
    super.initState();
    getCodes();
  }

  dispose() {
    super.dispose();
  }

  Future<Code> fetchCode(String code) async {
    final response = await http
        .get("https://apiqrcode.azurewebsites.net/api/reduction/code/" + code);

    if (response.statusCode == 200) {
      return Code.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load code');
    }
  }

  Future _scanQr() async {
    try {
      ScanResult qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult.rawContent;
      });
      fetchCode(result).then((result) {
        print(result.toString());
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailCode(),
                settings: RouteSettings(arguments: result)));
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "unknown error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown error $ex";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Liste des réductions"),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: codes.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(codes[index].code),
                  onTap: () {
                    print(codes[index].toString());
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailCode(),
                            settings: RouteSettings(arguments: codes[index])));
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                );
              },
            ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.camera_alt),
        label: Text("Scan"),
        onPressed: _scanQr,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class DetailCode extends StatelessWidget {
//  final Code code;
//
//  DetailCode({Key key, @required this.code}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Code code = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(code.code),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Date de début de la promo : " + code.dateDebut,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "Date de fin de la promo : " + code.dateFin,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "Description de la promo : " + code.description,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "Promotion : " + code.qRcode,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  QrImage(
                    data: code.code,
                    version: QrVersions.auto,
                    size: 300.0,
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
